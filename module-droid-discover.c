/***
  Copyright 2016 Simon Fels <simon.fels@canonical.com>
            2018 Marius Gripsgard <marius@ubports.com>
            2021 UBports Foundation.

  PulseAudio is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation; either version 2.1 of the
  License, or (at your option) any later version.

  PulseAudio is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with PulseAudio; if not, see <http://www.gnu.org/licenses/>.
***/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <pulsecore/core-util.h>
#include <pulsecore/macro.h>
#include <pulsecore/modargs.h>
#include <pulsecore/module.h>

#include <hybris/properties/properties.h>

PA_MODULE_AUTHOR("Ratchanan Srirattanamet");
PA_MODULE_DESCRIPTION("Detect on which Android version we're running and load correct implementation for it");
/* Module version must match PulseAudio version or newer PulseAudio will refuse
   to load the module. */
PA_MODULE_VERSION(PA_VERSION);
PA_MODULE_LOAD_ONCE(true);

#define ANDROID_PROPERTY_VERSION_SDK "ro.build.version.sdk"

#define MODULE_DROID_CARD_FORMAT "module-droid-card-%d"
#define MODULE_DROID_GLUE "module-droid-glue"
#define MODULE_DROID_GLUE_FORMAT "module-droid-glue-%d"
#define MODULE_DROID_HIDL "module-droid-hidl"
#define MODULE_DROID_HIDL_FORMAT "module-droid-hidl-%d"

#define AUDIOFLINGERGLUE_LIB32 "/system/lib/libaudioflingerglue.so"
#define AUDIOFLINGERGLUE_LIB64 "/system/lib64/libaudioflingerglue.so"

#define POST_JB2Q_MINIMUM_API_LEVEL 30

/* Copied from Pulseaudio 13.99.2. Remove when not support older version anymore. */
#ifndef PA_MODULE_ERR_SKIP
#define PA_MODULE_ERR_SKIP (2)
#endif

struct userdata {
    uint32_t module_idx;
    uint32_t af_module_idx;
};

static bool detect_audioflingerglue(void) {
    return access(AUDIOFLINGERGLUE_LIB32, F_OK) == 0 ||
           access(AUDIOFLINGERGLUE_LIB64, F_OK) == 0;
}

int pa__init(pa_module* m) {
    struct userdata *u;
    pa_module *mm;
    pa_modargs *ma;
    char version_str[64];
    int version;
    char module_name[64];
    bool found = false;
    char module_args[128] = "";
    int module_args_len = 0;
    char glue_args[128] = "";
    int glue_args_len = 0;
    char hidl_args[128] = "";
    int hidl_args_len = 0;
    const char* ma_key;
    const char* ma_value;
    void* ma_iter = NULL;
    bool has_audioflingerglue;
    const char * glue_or_hidl_module_name;

    pa_assert(m);

    if (property_get(ANDROID_PROPERTY_VERSION_SDK, version_str, "0") < 0) {
        pa_log("Failed to determine Android SDK version we're running on. "
               "Probably not an Android device.");
        return -PA_MODULE_ERR_SKIP;
    }

    /* In the recent time, module built against an older Android version tends
     * to also be usable on newer Android versions as well. So if module for
     * exact version isn't available, check the older ones as well.
     */
    for (version = atoi(version_str); version > 0; version--) {
        pa_snprintf(module_name, sizeof(module_name), MODULE_DROID_CARD_FORMAT,
                    version);

        if (pa_module_exists(module_name)) {
            found = true;
            break;
        }

        if (version == POST_JB2Q_MINIMUM_API_LEVEL) {
            /* pulseaudio-module-droid built from -jb2q codebase likely won't
             * be compatible with Android R (API level 30) and above.
             */
            break;
        }
    }

    if (!found) {
        pa_log("Unsupported Android version %s", version_str);
        return -PA_MODULE_ERR_SKIP;
    }

    /* parse the arguments to get hidl_args, then assemble the rest for passing
     * to the -droid module. */
    if ((ma = pa_modargs_new(m->argument, /* valid_keys - unspecified */ NULL)) == NULL) {
        pa_log_error("Cannot parse module arguments.");
        return -1;
    }

    while ((ma_key = pa_modargs_iterate(ma, &ma_iter)) != NULL) {
        ma_value = pa_modargs_get_value(ma, ma_key, /* default */ NULL);

        if (pa_streq(ma_key, "glue_args")) {
            pa_strlcpy(glue_args, ma_value, sizeof(glue_args));
            glue_args_len = strlen(glue_args);
        } else if (pa_streq(ma_key, "hidl_args")) {
            pa_strlcpy(hidl_args, ma_value, sizeof(hidl_args));
            hidl_args_len = strlen(hidl_args);
        } else {
            module_args_len += pa_snprintf(
                module_args + module_args_len,
                sizeof(module_args) - module_args_len,
                "%s%s=%s",
                module_args_len == 0 ? "" : " ",
                ma_key, ma_value);
            if (sizeof(module_args) - module_args_len <= 0) {
                pa_log_error("Module arguments too long.");
            }
        }
    }

#if ENABLE_DEEPBUFFER_WORKAROUND
    // Deep buffer audio outputs cause trouble with voice call volume
    // Use the quirks mechanism on older droid modules and options for newer modules.
    if (version <= 29) {
        module_args_len += pa_snprintf(
                module_args + module_args_len,
                sizeof(module_args) - module_args_len,
                "%s%s=%s",
                module_args_len == 0 ? "" : " ",
                "quirks", "-output_deep_buffer");
    } else {
        module_args_len += pa_snprintf(
                module_args + module_args_len,
                sizeof(module_args) - module_args_len,
                "%s%s=%s",
                module_args_len == 0 ? "" : " ",
                "output_deep_buffer", "false");
    }
#endif

    /* Accept additional arguments for -card, -glue, and -hidl module via
     * environment variables.
     */
    const char* extra_card_args = getenv("PULSE_MODULES_DROID_EXTRA_CARD_ARGS");
    if (extra_card_args) {
        module_args_len += pa_snprintf(
                module_args + module_args_len,
                sizeof(module_args) - module_args_len,
                "%s%s",
                module_args_len == 0 ? "" : " ",
                extra_card_args);
    }

    const char* extra_glue_args = getenv("PULSE_MODULES_DROID_EXTRA_GLUE_ARGS");
    if (extra_glue_args) {
        glue_args_len += pa_snprintf(
                glue_args + glue_args_len,
                sizeof(glue_args) - glue_args_len,
                "%s%s",
                glue_args_len == 0 ? "" : " ",
                extra_glue_args);
    }

    const char* extra_hidl_args = getenv("PULSE_MODULES_DROID_EXTRA_HIDL_ARGS");
    if (extra_hidl_args) {
        hidl_args_len += pa_snprintf(
                hidl_args + hidl_args_len,
                sizeof(hidl_args) - hidl_args_len,
                "%s%s",
                hidl_args_len == 0 ? "" : " ",
                extra_hidl_args);
    }

    pa_log_debug("Card module args: '%s'", module_args);
    pa_log_debug("Glue module args: '%s'", glue_args);
    pa_log_debug("HIDL module args: '%s'", hidl_args);

    m->userdata = u = pa_xnew0(struct userdata, 1);
    u->module_idx = PA_INVALID_INDEX;
    u->af_module_idx = PA_INVALID_INDEX;

    if (pa_module_load(&mm, m->core, module_name, module_args) < 0) {
        pa_log("Failed to load %s", module_name);
        goto fail;
    } else {
        u->module_idx = mm->index;
    }

    /* AudioFlingerglue or HIDL module */
    has_audioflingerglue = detect_audioflingerglue();

    glue_or_hidl_module_name =
        has_audioflingerglue ? MODULE_DROID_GLUE : MODULE_DROID_HIDL;

    if (!pa_module_exists(glue_or_hidl_module_name)) {
        pa_snprintf(module_name, sizeof(module_name),
                has_audioflingerglue
                    ? MODULE_DROID_GLUE_FORMAT
                    : MODULE_DROID_HIDL_FORMAT,
                version);
        if (pa_module_exists(module_name)) {
            glue_or_hidl_module_name = module_name;
        } else {
            pa_log("droid-%s module is not available for Android %d, ignored.",
                has_audioflingerglue ? "glue" : "hidl", version);
            goto done;
        }
    }

    /* We don't want to pass HIDL module's args to glue module, and vice-versa. */
    if (pa_module_load(&mm, m->core, glue_or_hidl_module_name,
            has_audioflingerglue ? glue_args : hidl_args) < 0) {
        pa_log("Failed to load %s", glue_or_hidl_module_name);
        goto fail;
    } else {
        u->af_module_idx = mm->index;
    }

done:
    pa_modargs_free(ma);
    return 0;

fail:
    if (ma)
        pa_modargs_free(ma);

    pa__done(m);
    return -1;
}

void pa__done(pa_module* m) {
    struct userdata *u;

    pa_assert(m);

    if (!(u = m->userdata))
        return;

    if (u->af_module_idx != PA_INVALID_INDEX)
        pa_module_unload_by_index(m->core, u->af_module_idx, true);

    if (u->module_idx != PA_INVALID_INDEX)
        pa_module_unload_by_index(m->core, u->module_idx, true);

    pa_xfree(u);
}
